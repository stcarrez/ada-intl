with "intl_config.gpr";
project Intl is

   type Yes_No is ("yes", "no");

   Use_NLS     : Yes_No := external ("INTL_USE_NLS", "yes");
   Use_Libintl : Yes_No := external ("INTL_USE_LIBINTL", "no");

   for Library_Name use "Intl";
   for Library_Version use Project'Library_Name & ".so." & Intl_Config.Crate_Version;

   for Source_Dirs use ("src/");
   for Object_Dir use "obj/" & Intl_Config.Build_Profile;
   for Create_Missing_Dirs use "True";
   for Library_Dir use "lib";

   type Library_Type_Type is ("relocatable", "static", "static-pic");
   Library_Type : Library_Type_Type :=
     external ("INTL_LIBRARY_TYPE", external ("LIBRARY_TYPE", "static"));
   for Library_Kind use Library_Type;

   package Naming is
      case Use_NLS is
         when "yes" =>
            for Implementation ("Intl")
               use "intl__nls.adb";

         when "no" =>
            for Implementation ("Intl")
               use "intl__none.adb";

      end case;
   end Naming;

   package Compiler is
      for Default_Switches ("Ada") use Intl_Config.Ada_Compiler_Switches;
   end Compiler;

   package Linker is
      case Use_Libintl is
         when "yes" =>
            for Default_Switches ("Ada") use Linker'Default_Switches ("Ada")
                & "-lintl";

         when "no" =>
            null;

      end case;
   end Linker;

   package Binder is
      for Switches ("Ada") use ("-Es"); --  Symbolic traceback
   end Binder;

   package Install is
      for Artifacts (".") use ("share");
   end Install;

end Intl;
